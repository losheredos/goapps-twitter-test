import axios from "axios/index";

export default function getUserTimelineAPI(userName) {
    return axios.get('https://cors-anywhere.herokuapp.com/https://gotl.me/api/get_userTimeline',{
        params:{
            name: userName
        }
    })
}
