import axios from "axios/index";

export default function getTweetsAPI() {
    return axios.get('https://cors-anywhere.herokuapp.com/https://gotl.me/api/get_tweets');
}
