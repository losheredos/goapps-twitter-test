import axios from "axios/index";

export default function searchTweetsAPI(hashtag, lang, amount, result_type) {
    return axios.get('https://cors-anywhere.herokuapp.com/https://gotl.me/api/search_tweets',{
        params:{
            q: hashtag,
            lang: lang,
            amount: amount,
            result_type: result_type
        }
    });
}
