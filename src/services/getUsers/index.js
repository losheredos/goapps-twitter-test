import axios from "axios/index";

export default function getUsersAPI() {
    return axios.get('https://cors-anywhere.herokuapp.com/https://gotl.me/api/get_users');
}

