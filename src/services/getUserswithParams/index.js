import axios from "axios/index";

export default function getUserswithParamsAPI(name, amount) {
    return axios.get('https://cors-anywhere.herokuapp.com/https://gotl.me/api/get_users',{
        params:{
            q: name,
            amount: amount,
        }
    });
}

