import getUsersAPI from './getUsers';
import getUserTimelineAPI from './getUserTimeline';
import getTweetsAPI from './getTweets';
import searchTweetsAPI from './searchTweets';
import getUserswithParamsAPI from './getUserswithParams';

export {
    getUsersAPI,
    getUserTimelineAPI,
    getTweetsAPI,
    searchTweetsAPI,
    getUserswithParamsAPI
}
