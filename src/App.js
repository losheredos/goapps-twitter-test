import React, { Component } from 'react';
import { MainContent, UsersPage} from './components'
// import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <div className="App">
            <Switch>
              <Route path="/" component={MainContent} exact />
              <Route path="/streaming" component={UsersPage} />
            </Switch>
          </div>
        </BrowserRouter>
    );
  }
}

export default App;


/*

<header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>

 */
