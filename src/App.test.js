import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/reducers'
import './index.css';
import App from './App';
import axios from "axios/index";

/*it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}> <App /> </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});*/

test('hello', () => {
  let a = 1;

  expect(a).toBe(1);
});

test('Header/clearAndChange', () => {
    const fetching = false;

    expect(fetching).toBeFalsy();
});

test('Header/changeFetch', () => {

    let opacity = 1;
    let interval = setInterval(() => {
        opacity -= 0.05;
        if(opacity < -0.2){
            clearInterval(interval);
            expect(opacity).toBeLessThan(0)
        }
    }, 25);

});

test('Header/showSearchDiv', () => {

    let opacity = 0;

    setInterval(() => {
        opacity += 0.02;
    }, 10);

    setTimeout(() => expect(opacity).toBeGreaterThan(0), 500);
});

test('Header/searchWithParams - API Test', () => {

    // Params may be sent even empty, these part handled in backend

    return axios.get('https://cors-anywhere.herokuapp.com/https://gotl.me/api/search_tweets',{
        params:{
            q: 'test',
            lang: 'en',
            amount: 22,
            result_type: ''
        }
    }).then(res => {
      expect(res.data).toBeDefined();
    }).catch(e => console.log(e));

});


