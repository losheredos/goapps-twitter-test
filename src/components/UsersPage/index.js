import React, {Component} from 'react';
import BottomBar from './BottomBar';
import Header from './Header';
import {connect} from "react-redux";
import {LoadingText} from '../AnimatedComponents';
import {getUsersAPI, getUserTimelineAPI} from '../../services';
import mapDispatchToProps from "../../redux/actions";
import {followers, following, favorite} from "./images";
import './usersPageStyles.css';
import '../MainContent/mainContentStyles.css';
import {retweet} from "../MainContent/images";

class UsersPage extends Component{

    constructor(){
        super();

        this.state = {
            loading: true
        }
    }

    componentDidMount() {
        this.getUsers();
    }

    getUsers(){
        let defaultAmounts = 20;

        getUsersAPI().then(res => {
            console.log(res);
            this.setState({loading: false});
            this.props.saveUsers(res.data, 'Warsaw', defaultAmounts, defaultAmounts);
        }).catch(e => console.log(e));
    }

    getUserTimeLine(userName){
        this.props.triggerRefreshing(true);

        getUserTimelineAPI(userName).then(res => {
            this.setState({timeline: true});
            this.props.saveTimeLineData(res.data, true);
            this.props.triggerRefreshing(false);
        }).catch(e => console.log(e));
    }

    renderUserList(){
        let {users} = this.props;
        let length = users.length;

        let mapped = users.map((val, index) => {
            return(
                <div style={{marginBottom: length == index+1 ? '6vh' : '0'}} key={index} className="usersContainer">
                    <img alt="profile pic" src={val.profile_image_url} className="profilePic"/>
                    <div className="profileTextContainer">
                        <b style={{textAlign: 'left', fontWeight: '500'}}>{index+1}. <b style={{color: '#14171a'}}> {val.name} </b> - <a style={{color: '#aab8c2'}} href={'https://twitter.com/@'+val.screen_name}> @{val.screen_name}</a> </b>

                        <small style={{textAlign: 'left'}}>{val.text}</small>

                        <div className="profileGetTimeLineContainer">
                            <div className="profileStatusImagesContainer">
                                <img alt="likes" src={favorite} className="favoriteImage"/>
                                <small className="statusCounterText">{val.favourites_count}</small>
                            </div>

                            <div className="profileStatusImagesContainer">
                                <img alt="likes" src={following} className="reTweetImage" />
                                <small className="statusCounterText">{val.friends_count}</small>
                            </div>

                            <div className="profileStatusImagesContainer">
                                <img alt="likes" src={followers} className="reTweetImage" />
                                <small className="statusCounterText">{val.followers_count}</small>
                            </div>
                        </div>

                        <b onClick={this.getUserTimeLine.bind(this, val.screen_name)} className="profileTimeLineButton">Go to Timeline</b>
                    </div>
                </div>
            )
        });

        return mapped;
    }

    renderDate(date){
        date = new Date(date);

        let secs = date.getSeconds();
        let mins = date.getMinutes();
        let hours = date.getHours();
        let days = date.getDate();
        let month = date.getMonth();
        let year = date.getFullYear();

        let lastDate;

        secs = secs < 10 ? '0'+secs : secs;
        mins = mins < 10 ? '0'+mins : mins;
        hours = hours < 10 ? '0'+hours : hours;
        days = days < 10 ? '0'+days : days;
        month = parseInt(month) + 1;
        month = month < 10 ? '0'+month : month;

        lastDate = hours+':'+mins+' '+days+'.'+month+'.'+year;

        return lastDate;
    }


    renderTimeLine(){
        let {timelineData} = this.props;
        let length = timelineData.length;

        let mapped = timelineData.map((val, index) => {
            let user = val.user;
            let date = this.renderDate(val.created_at);

            return(
                <div style={{marginBottom: length == index+1 ? '6vh' : '0'}} key={index} className="tweetsContainer">
                    <img alt="profile pic" src={user.profile_image_url} className="profilePic"/>
                    <div className="tweetTextContainer">
                        <b style={{textAlign: 'left', fontWeight: '500'}}>{index+1}. <b style={{color: '#14171a'}}> {user.name} </b> - <a style={{color: '#aab8c2'}} href={'https://twitter.com/@'+user.screen_name}> @{user.screen_name}</a> <small style={{color: '#aab8c2'}}>{date}</small> </b>

                        <small style={{textAlign: 'left'}}>{val.text}</small>
                        <div className="tweetStatusImagesContainer">
                            <div className="tweetStatusImagesContainerInner">
                                <img alt="likes" src={favorite} className="favoriteImage"/>
                                <small className="statusCounterText">{val.favorite_count}</small>
                            </div>

                            <div className="tweetStatusImagesContainerInner">
                                <img alt="likes" src={retweet} className="reTweetImage" />
                                <small className="statusCounterText">{val.retweet_count}</small>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        return mapped;
    }

    renderDisplayStates(){
        let {loading} = this.state;
        let {refreshing, timeline} = this.props;

        if(loading || refreshing === true){
            return <LoadingText text={'Loading'} />;
        }
        else if(timeline){
            return this.renderTimeLine();
        }
        else{
            return this.renderUserList();
        }
    }

    render(){
        return(
            <div className="mainContentContainer">
                <Header/>
                <div className="innerContainer">
                    {this.renderDisplayStates()}
                </div>
                <BottomBar/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return{
        users: state.users,
        refreshing: state.refreshing,
        timelineData: state.timelineData,
        timeline: state.timeline
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
