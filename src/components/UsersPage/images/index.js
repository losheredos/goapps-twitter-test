import followers from './followers.svg';
import following from './following.svg';
import favorite from './heart.svg';

export {
    followers,
    following,
    favorite
}
