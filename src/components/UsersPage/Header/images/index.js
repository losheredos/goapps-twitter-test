import pause from './pause.svg';
import play from './play.svg';
import user from './user.svg';
import logo from './logo.png';
import search from './search.svg';
import hashtag from './hashtag.svg';
import arrow from './arrow.svg';

export {
    user,
    play,
    pause,
    logo,
    search,
    hashtag,
    arrow
}
