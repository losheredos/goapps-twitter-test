import React, {Component} from 'react';
import {connect} from 'react-redux';
import './usersPageHeaderStyles.css';
import {NavLink} from 'react-router-dom';
import {search, logo, arrow, user} from './images';
import {getUserswithParamsAPI} from '../../../services';
import mapDispatchToProps from '../../../redux/actions';

class Header extends Component {

    constructor(props){
        super(props);

        this.state = {
            fetching: this.props.fetching ? this.props.fetching : true,
            fetchingText: 'Fetching'
        };

    }

    showSearchDiv(){
        this.props.changeFetch(false);
        let searchDiv = document.getElementById("searchDiv");
        searchDiv.style.display = "inline";
        let opacity = 0;

        this.opacityAnimation = setInterval(() => {
            opacity += 0.02;
            searchDiv.style.opacity = opacity;
            opacity > 1 && clearInterval(this.opacityAnimation);
        }, 10);
    }

    closeSearchDiv(search){
        let searchDiv = document.getElementById("searchDiv");
        let opacity = 1;

        this.opacityAnimation = setInterval(() => {
            opacity -= 0.02;
            searchDiv.style.opacity = opacity;
            if(opacity < -1){
                this.props.changeFetch(true);
                clearInterval(this.opacityAnimation);
                searchDiv.style.display = "none";
                search == 1 && this.searchWithParams()
            }
        }, 10);
    }

    searchWithParams(){
        let name = document.getElementById('name').value;
        let amount = document.getElementById('amount').value;

        this.props.triggerRefreshing(true);

        getUserswithParamsAPI(name, amount).then(res => {
            let received = res.data.length;
            this.props.saveUsers(res.data, name, amount, received);
            this.props.triggerRefreshing(false);
            this.props.saveTimeLineData([], false);
        }).catch(e => console.log(e));
    };

    render(){
        let {profileName} = this.props;

        return(
            <div className="Container">
                <div className="body">
                    <div className="iconsContainer">

                        <NavLink className="iconDiv" to="/">
                            <img alt="icon" src={arrow} className="icons" />
                            <b className="iconDivText">Home Page</b>
                        </NavLink>

                        <div onClick={this.showSearchDiv.bind(this)} className="searchIconDiv">
                            <img alt="icon" src={search} className="icons"/>
                            <b className="iconDivText">Search</b>
                        </div>

                        <div className="iconDiv">
                            <img alt="icon" src={user} className="icons" />
                            <b className="iconDivText">{profileName}</b>
                        </div>

                    </div>

                    <b className="goGo">Gogo Apps - Twitter Users & User Time Line API</b>

                    <img alt="logo" src={logo} style={{width: '7vw'}} />
                </div>

                <div id="searchDiv" className="searchPopup">
                    <div className="searchPopupInnerContainer">
                        <div className="searchPopupInnerDivs">
                            <small>Search User</small>
                            <input id="name" type="text"/>
                        </div>

                        <div className="searchPopupInnerDivs">
                            <small>Desired Result Amount</small>
                            <input id="amount" type="text"/>
                        </div>


                        <div className="searchPopupButtonDiv">
                            <button onClick={this.closeSearchDiv.bind(this)} style={{backgroundColor: '#657786'}} className="searchPopupButtons">Cancel</button>
                            <button onClick={this.closeSearchDiv.bind(this, 1)} className="searchPopupButtons">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return{
        fetching: state.fetching,
        searchText: state.hashtag,
        profileName: state.profileName
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
