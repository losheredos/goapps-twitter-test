import React, {Component} from 'react';
import {connect} from 'react-redux';
import mapDispatchToProps from '../../../redux/actions';
import './usersPageBottomStyles.css';
import {count, user, abacus} from "./images";

class BottomBar extends Component{

    render(){
        let {profileName, wantedUserAmount, receivedUserAndTweet} = this.props;

        return(
            <div className="bottomBarContainer">
                <div className="bottomIconsContainer">

                    <div className="bottomIconDiv">
                        <img alt="icon" src={user} className="icons"/>
                        <b className="bottomIconDivText">{profileName}</b>
                    </div>

                    <div className="bottomIconDiv">
                        <img alt="icon" src={count} className="icons"/>
                        <b className="bottomIconDivText">{receivedUserAndTweet}</b>
                    </div>

                    <div className="bottomIconDiv">
                        <img alt="icon" src={abacus} className="icons"/>
                        <b className="bottomIconDivText">{wantedUserAmount}</b>
                    </div>

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return{
        profileName: state.profileName,
        wantedUserAmount: state.wantedUserAmount,
        receivedUserAndTweet: state.receivedUserAndTweet
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BottomBar);
