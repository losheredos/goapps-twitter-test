import user from './user.svg';
import count from './count.svg';
import abacus from './abacus.svg';

export {
    count,
    abacus,
    user
}
