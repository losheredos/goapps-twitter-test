import React, {Component} from 'react';
import {connect} from 'react-redux';
import './headerStyles.css';
import {NavLink} from 'react-router-dom';
import {search, play, pause, logo, hashtag, arrow} from './images';
import mapDispatchToProps from '../../redux/actions';
import {searchTweetsAPI} from '../../services';

class Header extends Component {

constructor(props){
    super(props);

    this.state = {
        fetching: this.props.fetching ? this.props.fetching : true,
        fetchingText: 'Fetching'
    };

}

componentWillMount(){
    this.startFetchAnimation();
}

startFetchAnimation(){
    let counter = 0;

    this.fetchAnimation = setInterval(() => {
        let {fetchingText} = this.state;
        if(counter%4 === 0){
            this.setState({fetchingText: 'Fetching'});
        }
        else{
            this.setState({fetchingText: fetchingText+'.'});
        }
        counter++;
    }, 750)
}

changeFetch(){
    let settings = document.getElementById('settings');
    let {fetching} = this.props;

    let opacity = 1;
    this.animateTimer = setInterval(() => {
        settings.style.opacity = opacity;
        opacity -= 0.05;
        if(opacity < -0.2){
            this.clearAndChange();
            this.props.changeFetch(!fetching);
            setTimeout(() => settings.style.opacity = 1, 250);
        }
    }, 25);
}

clearAndChange(){
    const {fetching} = this.props;

    clearInterval(this.animateTimer);
    fetching ? clearInterval(this.fetchAnimation) : this.startFetchAnimation();
    this.setState({fetching: !fetching});
}

showSearchDiv(){
    this.props.changeFetch(false);
    let searchDiv = document.getElementById("searchDiv");
    searchDiv.style.display = "inline";
    let opacity = 0;

    this.opacityAnimation = setInterval(() => {
        opacity += 0.02;
        searchDiv.style.opacity = opacity;
        opacity > 1 && clearInterval(this.opacityAnimation);
    }, 10);
}

closeSearchDiv(search){
    let searchDiv = document.getElementById("searchDiv");
    let opacity = 1;

    this.opacityAnimation = setInterval(() => {
        opacity -= 0.02;
        searchDiv.style.opacity = opacity;
        if(opacity < -1){
            this.props.changeFetch(true);
            clearInterval(this.opacityAnimation);
            searchDiv.style.display = "none";
            search == 1 && this.searchWithParams()
        }
    }, 10);
}

searchWithParams(){
    let hashtag = document.getElementById('hashtag').value;
    let lang = document.getElementById('lang').value;
    let amount = document.getElementById('amount').value;
    let result_type = document.getElementById('result_type').value;

    this.props.triggerRefreshing(true);

    searchTweetsAPI(hashtag, lang, amount, result_type).then(res => {
        this.props.saveTweets(res.data.statuses, res.data.statuses.length);
        this.props.triggerRefreshing(false);

        let paramsArray = [];
        paramsArray.push(hashtag);
        paramsArray.push(lang);
        paramsArray.push(amount);
        paramsArray.push(result_type);
        this.props.saveSearchParams(paramsArray, hashtag);
    }).catch(e => console.log(e));
};

render(){
    const {fetchingText} = this.state;
    let {searchText, fetching} = this.props;

    return(
        <div className="Container">
            <div className="body">
                <div className="iconsContainer">
                <div onClick={this.showSearchDiv.bind(this)} className="searchIconDiv">
                    <img alt="icon" src={search} className="icons"/>
                    <b className="iconDivText">Search</b>
                </div>

                <div className="iconDiv">
                    <img alt="icon" src={hashtag} className="icons" />
                    <b className="iconDivText">{searchText.length == 0 ? 'No Hashtag' : searchText}</b>
                </div>

                <div onClick={this.changeFetch.bind(this)} id="settings" className="fetchIconDiv">
                    <img alt="icon" src={fetching ? play : pause} className="icons" />
                    <b className="iconDivText">{fetching ? fetchingText : 'Paused'}</b>
                </div>

                </div>

                <b className="goGo">Gogo Apps - Twitter Streaming Search API</b>

                <NavLink className="linkingDiv" to="/streaming">
                    <img alt="icon" src={arrow} className="icons" />
                    <b className="iconDivText">User Time Line</b>
                </NavLink>

                <img alt="logo" src={logo} style={{width: '7vw'}} />
            </div>

            <div id="searchDiv" className="searchPopup">
                <div className="searchPopupInnerContainer">
                    <div className="searchPopupInnerDivs">
                        <small>Search Hashtag</small>
                        <input id="hashtag" type="text"/>
                    </div>

                    <div className="searchPopupInnerDivs">
                        <small>Desired Language(as ISO code)</small>
                        <input id="lang" type="text" placeholder=""/>
                    </div>

                    <div className="searchPopupInnerDivs">
                        <small>Desired Tweet Amount</small>
                        <input id="amount" type="text"/>
                    </div>

                    <div className="searchPopupInnerDivs">
                        <small>Result Type</small>
                        <select id="result_type">
                            <option value="popular">Popular</option>
                            <option value="recent">Recent</option>
                            <option value="mixed">Mixed</option>
                        </select>

                    </div>

                    <div className="searchPopupButtonDiv">
                        <button onClick={this.closeSearchDiv.bind(this)} style={{backgroundColor: '#657786'}} className="searchPopupButtons">Cancel</button>
                        <button onClick={this.closeSearchDiv.bind(this, 1)} className="searchPopupButtons">Search</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
}

function mapStateToProps(state) {
    return{
        fetching: state.fetching,
        searchText: state.hashtag
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
