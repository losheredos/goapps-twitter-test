import pause from './pause.svg';
import play from './play.svg';
import settings from './settings.svg';
import logo from './logo.png';
import search from './search.svg';
import hashtag from './hashtag.svg';
import arrow from './arrow.svg';

export {
    settings,
    play,
    pause,
    logo,
    search,
    hashtag,
    arrow
}
