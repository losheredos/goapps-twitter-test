import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, BottomBar} from '../index';
import {favorite, retweet} from './images';
import {LoadingText} from '../AnimatedComponents';
import './mainContentStyles.css';
import {getTweetsAPI, searchTweetsAPI} from '../../services';
import mapDispatchToProps from '../../redux/actions';

class MainContent extends Component{

    constructor(){
        super();

        this.state = {
            lastTweets: [],
            loading: true
        };
    }

    componentDidMount() {
        this.startFetching();
    }

    componentWillUnmount(){
        clearInterval(this.tweetInterval);
    }

    startFetching(){
        this.getTweets();

        this.tweetInterval = setInterval(() => {
            if(this.props.fetching){
                this.props.triggerRefreshing(true);
                this.getTweets();
            }
        }, 10000);
    }

    getTweets(){
        let {searchParams} = this.props;

        if(searchParams.length > 0){
            let hashtag = searchParams[0];
            let lang = searchParams[1];
            let amount = searchParams[2];
            let result_type = searchParams[3];

            searchTweetsAPI(hashtag, lang, amount, result_type).then(res => {
                this.props.saveTweets(res.data.statuses, res.data.statuses.length);
                this.props.triggerRefreshing(false);
            }).catch(e => console.log(e));
        }

        else{
            getTweetsAPI().then(res => {
                console.log(res);
                this.props.saveTweets(res.data.statuses, res.data.statuses.length);
                this.props.triggerRefreshing(false);
                this.setState({loading: false})
            }).catch(e => console.log(e));
        }
    }

    /*calculateTimeDiff(tweetDate){
        let now = new Date();

        now = now.getTime();
        tweetDate = new Date(tweetDate).getTime();

        let difference = Math.abs(tweetDate - now);
        let diffInMins = Math.round(((difference % 86400000) % 3600000) / 60000);

        return diffInMins;
    }*/

    renderDate(date){
        date = new Date(date);

        let secs = date.getSeconds();
        let mins = date.getMinutes();
        let hours = date.getHours();
        let days = date.getDate();
        let month = date.getMonth();
        let year = date.getFullYear();

        let lastDate;

        secs = secs < 10 ? '0'+secs : secs;
        mins = mins < 10 ? '0'+mins : mins;
        hours = hours < 10 ? '0'+hours : hours;
        days = days < 10 ? '0'+days : days;
        month = parseInt(month) + 1;
        month = month < 10 ? '0'+month : month;

        lastDate = hours+':'+mins+' '+days+'.'+month+'.'+year;

        return lastDate;
    }

    renderLastTweets(){
        let {tweets} = this.props;
        let length = tweets.length;

        let mapped = tweets.map((val, index) => {
            let user = val.user;
            //let minAgo = this.calculateTimeDiff(val.created_at);
            //minAgo = minAgo == 0 ? 'Just Now' : minAgo + ' min ago';
            let date = this.renderDate(val.created_at);

            return(
                <div style={{marginBottom: length == index+1 ? '6vh' : '0'}} key={index} className="tweetsContainer">
                    <img alt="profile pic" src={user.profile_image_url} className="profilePic"/>
                    <div className="tweetTextContainer">
                        <b style={{textAlign: 'left', fontWeight: '500'}}>{index+1}. <b style={{color: '#14171a'}}> {user.name} </b> - <a style={{color: '#aab8c2'}} href={'https://twitter.com/@'+user.screen_name}> @{user.screen_name}</a> <small style={{color: '#aab8c2'}}>{date}</small> </b>

                        <small style={{textAlign: 'left'}}>{val.text}</small>
                        <div className="tweetStatusImagesContainer">
                            <div className="tweetStatusImagesContainerInner">
                            <img alt="likes" src={favorite} className="favoriteImage"/>
                            <small className="statusCounterText">{val.favorite_count}</small>
                            </div>

                            <div className="tweetStatusImagesContainerInner">
                            <img alt="likes" src={retweet} className="reTweetImage" />
                            <small className="statusCounterText">{val.retweet_count}</small>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        return mapped
    }

    renderDisplayStates(){
        const {loading} = this.state;
        let {refreshing} = this.props;

        console.log(this.props);

        if(loading || refreshing == true){
            return <LoadingText text={'Loading'} />;
        }
        else{
            return this.renderLastTweets()
        }
    }

    render(){


        return(
            <div className="mainContentContainer">
                <Header/>
                    <div className="innerContainer">
                        {this.renderDisplayStates()}
                    </div>
                <BottomBar/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return{
        fetching: state.fetching,
        refreshing: state.refreshing,
        tweets: state.tweets,
        searchParams: state.searchParams
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContent);
