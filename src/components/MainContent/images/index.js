import favorite from './heart.svg';
import retweet from './retweet.svg';

export {
    retweet,
    favorite
}
