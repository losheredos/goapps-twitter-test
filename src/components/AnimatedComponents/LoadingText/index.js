import React, {Component} from 'react';

class LoadingText extends Component{

    constructor(props){
        super(props);

        this.state = {
            text: this.props.text
        };
    }

    componentWillMount(){
        this.renderText();
    }

    componentWillUnmount(){
        clearInterval(this.timer);
    }

    renderText(){
        let initialText = this.props.text;
        let counter = 0;

        this.timer = setInterval(() => {
            let {text} = this.state;

            if(counter%4 === 0){
                this.setState({text: initialText})
            }
            else{
                this.setState({text: text+'.'})
            }
            counter++;
        }, 250);
    }

    render(){
        return(
            <div style={{marginTop: '40vh'}}>
            <b>{this.state.text}</b>
            </div>
        );
    }
}

export default LoadingText;
