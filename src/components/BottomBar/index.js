import React, {Component} from 'react';
import {tag, on, off, list, count, abacus} from './images';
import {connect} from 'react-redux';
import mapDispatchToProps from '../../redux/actions';
import './bottomBarStyles.css';

class BottomBar extends Component{

    render(){
        let {fetching, tweetsAmount, hashtag, category, desiredAmount} = this.props;

        return(
            <div className="bottomBarContainer">
                <div className="bottomIconsContainer">

                    <div className="bottomIconDiv">
                        <img alt="icon" src={fetching ? on : off} className="icons"/>
                        <b className="bottomIconDivText">{fetching ? 'Online' : 'Offline'}</b>
                    </div>

                    <div className="bottomIconDiv">
                        <img alt="icon" src={count} className="icons"/>
                        <b className="bottomIconDivText">{tweetsAmount}</b>
                    </div>

                    <div className="bottomIconDiv">
                        <img alt="icon" src={list} className="icons"/>
                        <b className="bottomIconDivText">{category ? category : 'No Category'}</b>
                    </div>

                    <div className="bottomIconDiv">
                        <img alt="icon" src={abacus} className="icons"/>
                        <b className="bottomIconDivText">{desiredAmount ? desiredAmount : '50'}</b>
                    </div>

                    <div className="bottomIconDiv">
                        <img alt="icon" src={tag} className="icons"/>
                        <b className="bottomIconDivText">{hashtag.length == 0 ? 'No Hashtag' : hashtag}</b>
                    </div>

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return{
        fetching: state.fetching,
        tweetsAmount: state.tweetsAmount,
        hashtag: state.hashtag,
        category: state.searchParams[3],
        desiredAmount: state.searchParams[2]
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BottomBar);
