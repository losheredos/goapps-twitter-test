import list from './list.svg';
import on from './on.svg';
import off from './off.svg';
import tag from './tag.svg';
import count from './count.svg';
import abacus from './abacus.svg';

export {
    list,
    off,
    on,
    tag,
    count,
    abacus
}
