import {createStore} from 'redux';

const initialState = {
    fetching: true,
    refreshing: false,
    hashtag: 'World',
    profileName: 'Warsaw',
    users: [],
    wantedUserAmount: 0,
    receivedUserAndTweet: 0,
    tweets: [],
    searchParams: [],
    timelineData: '',
    timeline: false,
    tweetsAmount: 0
};

const rootReducer = (state = initialState, action) => {

    switch (action.type){

        case 'CHANGE_FETCH': return {...state, fetching: action.payload};

        case 'REFRESHING': return {...state, refreshing: action.payload};

        case 'SAVE_TWEETS': return {...state, tweets: action.payload, tweetsAmount: state.tweetsAmount+action.payload_2};

        case 'SAVE_SEARCH_PARAMS': return {...state, searchParams: action.payload, hashtag: action.payload_2};

        case 'SAVE_USERS': return {...state, users: action.payload, profileName: action.payload_2, wantedUserAmount: action.payload_3, receivedUserAndTweet: state.receivedUserAndTweet + action.payload_4};

        case 'SAVE_TIMELINE_DATA': return {...state, timelineData: action.payload, timeline: action.payload_2};

        default: return state;
    }

};

let store = createStore(rootReducer);
export default store;
