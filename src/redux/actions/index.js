function mapDispatchToProps(dispatch) {
    return{
        changeFetch: (boolean) => dispatch({type: 'CHANGE_FETCH', payload: boolean}),

        triggerRefreshing: (boolean) => dispatch({type: 'REFRESHING', payload: boolean}),

        saveTweets: (tweets, tweetsAmount) => dispatch({type: 'SAVE_TWEETS', payload: tweets, payload_2: tweetsAmount}),

        saveSearchParams: (params, hashtag) => dispatch({type: 'SAVE_SEARCH_PARAMS', payload: params, payload_2: hashtag}),

        saveUsers: (users, profileName, wantedAmount, receivedAmount) => dispatch({type: 'SAVE_USERS', payload: users, payload_2: profileName, payload_3: wantedAmount, payload_4: receivedAmount}),

        saveTimeLineData: (data, boolean) => dispatch({type: 'SAVE_TIMELINE_DATA', payload: data, payload_2: boolean})
    }
}

export default mapDispatchToProps;
